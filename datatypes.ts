
interface BusinessData {

    _id: string,
    businesDetailsAdded: boolean,
    businessCategory: string,
    businessKycPhoto: string,
    businessKycType: string,
    businessMobile: string,
    businessName: string,
    businessQR: string,
    createdAt: string,
    deliveryTime: string,
    kycAdded: boolean,
    location_name: string,
    location_status: boolean,
    paymentAdded: boolean,
    paymentMode: string | null,
    receivePaymentUPI: string | null,
    status: boolean,
    updatedAt: string,
    vendor: string,
    vendorKycPhoto: string,
    vendorKycType: string,
    workingHours: WorkingHours
}
interface WorkingHours {
    end: string,
    start: string,
}

interface ServerResponse {
    data: ServerData
}
interface fetchProductsParams {
    number: number
}

interface ChatMessage {
    from: string, to: string, room: string, message: string, _id: string
}
interface RoomListMessages {
    room: string, list: ChatMessage[]
}

interface SendMessage {
    room: string, message: string, to: string
}

interface ServerData {
    message: string
    status: number,
    otp?: string,
    data: { user: User },
    user: User,
    token: string,
    isRegister: boolean
}
interface CustomServerData<T> {
    message: string
    status: number,

    data: T
}

interface Interest {
    message: string
    status: number,
    data: []
}
interface UserData {
    message: string
    status: number,
    data: User
}

interface loc {
    type: { type: String },
    coordinates: number[],
}

interface User {
    user_email: string,
    user_mobile: string,
    user_name: string,
    user_interest: string[],
    mobile_verified: boolean,
    location_status: boolean,
    userCurrent_location: loc,
    uri: string,
    _id: string,
    productLimit: number,

}


interface Matches {
    _id: string,
    users: User[],
    isMatched: boolean,
    user: User


}

interface ProductUpdateParameters {
    id: string, file: any, title: string, discription: string, productType: string, prefered: string, location: any
}

interface ServerProduct {
    _id: string,
    user_id: string,
    product_url: string,
    prefered_product: string,
    product_type: string,
    product_description: string,
    product_title: string,
    location: loc,
    isLiked: boolean,
}




interface LikeeUser {
    _id: string,
    user_name: string,
    uri: string,
}

interface LikeeUserBlob {
    _id: string,
    user_id: LikeeUser,

}
interface ServerProductOf<T> {
    _id: string,
    user_id: T,
    product_url: string,
    prefered_product: string,
    product_type: string,
    product_description: string,
    product_title: string,
    location: loc,
    distance?: number,
    isLiked: boolean
}
interface ServerPageListProduct {
    page: number,
    list: ServerProductOf<User>[]
}

interface Like {
    liked: boolean,
    matched: boolean
}


interface product {

    uri: IValidObjectType<string>,
    title: IValidObjectType<string>,
    discription: IValidObjectType<string>,
    productType: IValidObjectType<string>,
    prefered: IValidObjectType<string>,
    location: IValidObjectType<number[]>,

    isValidProduct: boolean,



}

interface IValidObjectType<T> {
    value: T, validation: IValidObject
}



interface IValidObject {

    isValid: boolean,
    message: string,


}
interface postProduct { file: any, title: string, discription: string, productType: string, prefered: string, location: any }

export {
    User,
    loc,
    UserData,
    Interest,
    ServerData,
    ServerResponse,
    CustomServerData,

    product,
    IValidObject,
    postProduct,
    ServerProduct,
    ProductUpdateParameters,
    fetchProductsParams,
    ServerPageListProduct,
    ServerProductOf,
    Like,


    LikeeUserBlob,
    LikeeUser,

    Matches,
    ChatMessage,
    RoomListMessages,
    SendMessage,
    BusinessData

}


