import React from "react";
import { useFonts } from "@use-expo/font";
import { NavigationContainer } from "@react-navigation/native";
import Root from "./src/Navigation/Root";
import { Provider, store } from './redux/index'

export default function App() {
  const [isLoaded] = useFonts({
    "Raleway-Thin": require("./assets/fonts/Raleway-Thin.ttf"),
    "Raleway-Bold": require("./assets/fonts/Raleway-Bold.ttf"),
    "Raleway-Medium": require("./assets/fonts/Raleway-Medium.ttf"),
    "Raleway-Regular": require("./assets/fonts/Raleway-Regular.ttf"),
    "AbrilFatface-Regular": require("./assets/fonts/AbrilFatface-Regular.ttf"),
  });

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Root />
      </NavigationContainer>
    </Provider>
  );
}
