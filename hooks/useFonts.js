import * as Font from "expo-font";
import {
  Montserrat_300Light,
  Montserrat_400Regular,
  Montserrat_500Medium,
  Montserrat_700Bold,
  Montserrat_600SemiBold,
  Montserrat_100Thin
} from "@expo-google-fonts/montserrat";

const useFonts = async () => {
  await Font.loadAsync({
    Montserrat_Light: Montserrat_300Light,
    Montserrat_Regular: Montserrat_400Regular,
    Montserrat_Medium: Montserrat_500Medium,
    Montserrat_Bold: Montserrat_700Bold,
    Montserrat_SemiBold: Montserrat_600SemiBold,
    Montserrat_Thin:Montserrat_100Thin,
  });
};

export default useFonts;
