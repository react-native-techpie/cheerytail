
import { reducer as AuthReducer } from './Auth/reducers';
import { reducer as HomeReducer } from './Home/reducer';

import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux'

const rootReducer = combineReducers({
    Auth: AuthReducer,
    Home: HomeReducer,

});

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

export { store, Provider }