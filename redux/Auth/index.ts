


import { connect } from 'react-redux';

import { actionCreators as actions } from './actions';


function mapStateToProps(state: any) {
    const {
        isLoading,
        isSignout,
        userToken,
        email,
        phone,
        password,
        name,
        isValidRegisterForm,
        isValidLoginForm,
        responseError,
        otp,
        passwordResetInput,
        resetPasswordInput,
        user,

        emailOrPassword,

    } = state.Auth;
    return {
        isLoading,
        isSignout,
        userToken,
        email,
        phone,
        password,
        name,
        isValidRegisterForm,
        isValidLoginForm,
        responseError,
        otp,
        passwordResetInput,
        resetPasswordInput,
        user,
        emailOrPassword
    };
}



export { connect, actions, mapStateToProps }