
import {
    SAVE_INTERESTS,
    RESET_PASSWORDS,
    EMAIL_OR_PASSWORD,
    RESTORE_TOKEN,
    SIGN_IN,
    SIGN_OUT,
    SAVE_OTP,
    EMAIL_VALIDATION_ERROR,
    PHONE_VALIDATION_ERROR,
    PASSWORD_VALIDATION_ERROR,
    NAME_VALIDATION_ERROR,
    REGISTER_FORM_VALIDATION,
    REGISTER_FORM_RESPONSE,
    UPDATE_USER
} from './types';

import {
    TIMER_STOP,
    TIMER_START,
    TIMER_TICK
} from './types'


// initial state
const initialState = {
    isLoading: true,
    isSignout: false,
    userToken: null,
    email: { isValid: false, error: '', email: '' },
    phone: { isValid: false, error: '', phone: '' },
    password: { isValid: false, error: '', password: '' },
    name: { isValid: true, error: '', name: '' },
    isValidRegisterForm: false,
    isValidLoginForm: false,
    responseError: "",
    otp: '',
    emailOrPassword: { value: '', type: '', error: '', isValid: false },
    resetPasswordInput: { value: '', error: '', isValid: false },
    user: null,
    interests: []
};

// Helper Functions

const interests = ({ state, action }: any) => {
    return {
        ...state,
        interests: action.payload
    };
}


const updateUser = ({ state, action }: any) => {
    return {
        ...state,
        user: action.payload
    };
}

const restoreToken = ({ state, action }: any) => {
    return {
        ...state,

        userToken: action.payload,

    };
}

const signIn = ({ state, action }: any) => {
    return {
        ...state,
        isSignout: false,
        userToken: action.payload.token,
        user: action.payload.user,
    };
}

const signOut = ({ state, action }: any) => {
    return initialState
}

const isValidRegisterForm = (state: any) => {

    return state.email.isValid && state.password.isValid && state.name.isValid;
}

const isValidLoginForm = (state: any) => {

    return state.email.isValid && state.password.isValid;
}

const ValidateRegisterForm = ({ state, action }: any) => {
    const isValidRegister = isValidRegisterForm(state)
    const isValidLogin = isValidLoginForm(state)

    return {
        ...state,
        responseError: "",
        isValidRegisterForm: isValidRegister,
        isValidLoginForm: isValidLogin,
    };

}

const emailValidationError = ({ state, action }: any) => {

    return {
        ...state,
        email: action.payload,

    };
}

const phoneValidationError = ({ state, action }: any) => {
    return {
        ...state,
        phone: action.payload,

    };
}

const passwordValidationError = ({ state, action }: any) => {

    return {
        ...state,
        password: action.payload,

    };
}

const nameValidationError = ({ state, action }: any) => {


    return {
        ...state,
        name: action.payload,
    };
}

const formrResponseError = ({ state, action }: any) => {
    return {
        ...state,
        responseError: action.payload,

    };
}

// Reducer Function

const SaveOTP = ({ state, action }: any) => {
    return {
        ...state,
        otp: action.payload,
    };
}

const setEmailOrPassowrd = ({ state, action }: any) => {
    return {
        ...state,
        emailOrPassword: action.payload,
    };
}

const resetPasswordsValidation = ({ state, action }: any) => {

    return {
        ...state,
        resetPasswordInput: action.payload,
    };
}




const tick = ({ state, action }: any) => {

    const newTik = state.timer - 1;
    if (newTik <= 0) {
        return stopTimer({ state, action });
    }
    return {
        ...state,
        timer: state.timer - 1,

    }
}



const stopTimer = ({ state, action }: any) => {

    return {
        ...state,
        timer: 0,
        isTimer: false

    }
}


const startTimer = ({ state, action }: any) => {

    return {
        ...state,
        timer: action.payload,
        isTimer: true
    }
}

function reducer(state = initialState, action: any) {
    switch (action.type) {
        case RESTORE_TOKEN:
            return restoreToken({ state, action });
        case SIGN_IN:
            return signIn({ state, action });
        case SIGN_OUT:
            return signOut({ state, action })

        case EMAIL_VALIDATION_ERROR:
            return emailValidationError({ state, action })
        case PHONE_VALIDATION_ERROR:
            return phoneValidationError({ state, action })
        case PASSWORD_VALIDATION_ERROR:
            return passwordValidationError({ state, action })
        case NAME_VALIDATION_ERROR:
            return nameValidationError({ state, action })
        case REGISTER_FORM_VALIDATION:
            return ValidateRegisterForm({ state, action })
        case REGISTER_FORM_RESPONSE:
            return formrResponseError({ state, action })

        case SAVE_OTP:
            return SaveOTP({ state, action })

        case EMAIL_OR_PASSWORD:
            return setEmailOrPassowrd({ state, action })

        case RESET_PASSWORDS:
            return resetPasswordsValidation({ state, action })

        case SAVE_INTERESTS:
            return interests({ state, action })

        case UPDATE_USER:
            return updateUser({ state, action })
        /*  TIMER_STOP,
            TIMER_START,
            TIMER_TICK
        case TIMER_TICK:
            return tick({ state, action })
        case TIMER_START:
            return startTimer({ state, action })
        case TIMER_STOP:
            return stopTimer({ state, action })
            */
        default:

            return state;
    }
}

export { reducer };

