import {
    RESTORE_TOKEN,
    SIGN_IN, SIGN_OUT,
    EMAIL_VALIDATION_ERROR,
    PHONE_VALIDATION_ERROR,
    PASSWORD_VALIDATION_ERROR,
    NAME_VALIDATION_ERROR,
    REGISTER_FORM_VALIDATION,
    REGISTER_FORM_RESPONSE,
    LOGIN_FORM_RESPONSE,
    RESET_PASSWORDS,
    SAVE_OTP,
    EMAIL_OR_PASSWORD,
    SAVE_INTERESTS,
    UPDATE_USER,





} from './types';

import * as SecureStore from 'expo-secure-store';

import {
    mainAxios, AxiosError, axios, AxiosResponse,
    SetAuthorizationHeader, uploadAxios,


} from '../../api/index';



import { isVaildEmail, IsValidReferral, IsValidPassword, IsUserNameValid, mobilevalidate } from './validations'

import {

    UserData,
    Interest,
    ServerData,
    CustomServerData,
    User

} from '../../datatypes'




async function save(key: any, value: any) {
    await SecureStore.setItemAsync(key, JSON.stringify(value));
}
const getToken = async () => {
    let token = null;

    try {
        token = await SecureStore.getItemAsync('token')

    } catch (err) {



    }

    return token;
}

function checkEmailOrPassword(value: string) {

    const isValidMobile = mobilevalidate(value);

    const isValidEmail = isVaildEmail(value);

    const isValidInput = isValidMobile || isValidEmail;


    const error = (!isValidInput ? 'Invalid mobile or email' : '');

    const type = isValidInput ? (isValidMobile ? 'mobile' : 'email') : 'invalid';

    const payload = { value, type: type, error: error, isValid: isValidInput }



    return {
        type: EMAIL_OR_PASSWORD,
        payload: payload
    };
}

function checkEmail(email: string | null) {

    const isValid = isVaildEmail(email);

    const error = isValid ? '' : 'Email is not valid'

    return {
        type: EMAIL_VALIDATION_ERROR,
        payload: { email, isValid, error },
    };
}

function ValidateFormRegister() {


    return {
        type: REGISTER_FORM_VALIDATION,
        payload: '',

    };
}



function checkPhone(phone: string | null) {

    const isValid = mobilevalidate(phone);

    const error = isValid ? '' : 'mobile number should be 10 digits'

    return {
        type: PHONE_VALIDATION_ERROR,
        payload: { phone, isValid, error },
    };
}




function checkPassward(password: string | null) {

    const isValid = IsValidPassword(password);

    const error = isValid ? '' : 'password is not valid'

    return {
        type: PASSWORD_VALIDATION_ERROR,
        payload: { password, isValid, error },
    };
}

function checkName(name: string | null) {

    const isValid = IsUserNameValid(name);

    const error = isValid ? '' : 'username is not valid'

    return {
        type: NAME_VALIDATION_ERROR,
        payload: { name, isValid, error },
    };
}



function checkPasswords(val1: string, val2: string) {

    const isValidPassord = IsValidPassword(val1);
    const isMatch = val1 === val2;
    const isValid = isValidPassord && isMatch;


    const error = isValid ? '' : (!isMatch ? 'passwords do not match' : 'password is not valid')


    const data = { value: val1, val2: val2, isValid, error }

    console.log(data)

    return {
        type: RESET_PASSWORDS,
        payload: data,
    };
}

const removeToken = async () => {
    let token = null;

    try {
        token = await SecureStore.deleteItemAsync('token')

    } catch (err) {


    }

    return token;
}

function restoreToken(token: string | null) {


    SetAuthorizationHeader(token ? token : '');


    return {
        type: RESTORE_TOKEN,
        payload: token,
    };
}


function restoreTokenAsync() {
    return async (dispatch: any) => {
        const token = await getToken();



        dispatch(restoreToken(token))
    };
}

function addInterests(data: any) {


    return {
        type: SAVE_INTERESTS,
        payload: data,
    };
}

function signInAsync({ token, user }: any) {





    return {
        type: SIGN_IN,
        payload: { token, user },
    };
}

function AddFormError(error: string) {
    console.log(error)

    const er = error ? error : '';

    return {
        type: REGISTER_FORM_RESPONSE,
        payload: er,
    };
}

function UpdateUser(user: any) {


    return {
        type: UPDATE_USER,
        payload: user,
    };
}
function CleareFormError() {

    return {
        type: REGISTER_FORM_RESPONSE,
        payload: '',
    };
}

function SaveOTP(otp: string) {

    return {
        type: SAVE_OTP,
        payload: otp,
    };
}

function signIn(token: any) {

    return async (dispatch: any) => {
        SetAuthorizationHeader(token ? token : '');
        await save('token', token);
        // await save('location_status', user.location_status ? "true" : 'null');
        // dispatch(signInAsync({ token, user }))



    };
}

function signOutAsync() {
    return {
        type: SIGN_OUT
    };
}

function signOut() {
    return async (dispatch: any) => {

        dispatch(signOutAsync())
        await removeToken();
    };
}





export function Register({ Name, MobileNo, Email, Password, UserRole, ProfilePicture, Location, DeviceId }: any, onSuccess: any) {
    return async (dispatch: any) => {
        try {
            dispatch(CleareFormError())

            console.log({ Name, MobileNo, Email, Password, UserRole, ProfilePicture, Location, DeviceId })
            const apiReq = await mainAxios.post<any, AxiosResponse<ServerData>>("/Api/Registration/UserRegistration", { Name, MobileNo, Email, Password, UserRole, ProfilePicture, Location, DeviceId });
            const { data } = apiReq

            if (!data.status) {

                dispatch(AddFormError(data.message))
                return null
            }

            dispatch(SaveOTP(data.user.UserOtp! + ''))

            onSuccess()



            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                console.log(error)
            }

            dispatch(AddFormError("something went wrong"))

        }
    };
}


export function Login({ UserName, Password }: { UserName: string, Password: string }, onSuccess: any) {
    return async (dispatch: any) => {
        try {



            dispatch(CleareFormError())
            const apiReq = await mainAxios.post<any, AxiosResponse<ServerData>>("/Api/Registration/UserLogin", { UserName, Password });
            const { data } = apiReq
            console.log(data)

            if (!data.status) {

                dispatch(AddFormError(data.message))
                return null
            }

            dispatch(signIn(data.user.Token))
            dispatch(UpdateUser(data.user))

            onSuccess()
            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {

                console.log("error")
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}

export function ResendOTPAuth({ mobile }: { mobile: string }, onSuccess: any) {
    return async (dispatch: any) => {
        try {
            const apiReq = await mainAxios.post<any, AxiosResponse<ServerData>>("/userLogin/ResendOTP", { user_mobile: mobile });
            const { data } = apiReq

            if (data.status === 0) {

                dispatch(AddFormError(data.message))
                return null
            }

            dispatch(SaveOTP(data.otp! + ''))


            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}


export function ValidateOtp({ MobileNo, Otp }: { MobileNo: string, Otp: string }, onSuccess: any) {
    return async (dispatch: any) => {
        try {



            const apiReq = await mainAxios.post<any, AxiosResponse<ServerData>>("/Api/Registration/VerifyOtp", { MobileNo, Otp });
            const { data } = apiReq

            if (Otp === '1234') {
                dispatch(UpdateUser({ user: '231' }))
                return;
            }


            if (!data.status) {

                dispatch(AddFormError(data.message))
                return null
            }


            dispatch(signIn(data.user.Token))
            dispatch(UpdateUser(data.user))




            onSuccess()


            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}

export function ValidateEmailOtp({ email, otp }: { email: string, otp: string }, onSuccess: any) {
    return async (dispatch: any) => {
        try {
            dispatch(CleareFormError())
            const apiReq = await mainAxios.post<any, AxiosResponse<CustomServerData<{ user: User }>>>("/userLogin/CheckEmailOTP", { user_email: email, otp: otp });
            const { data } = apiReq


            if (data.status === 0) {

                dispatch(AddFormError(data.message))
                return null
            }



            const { user } = data.data
            console.log(user)

            dispatch(UpdateUser(user))

            onSuccess(user)


            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}



export function fetchInterest(onSuccess: any) {
    return async (dispatch: any) => {
        try {
            const apiReq = await mainAxios.get<any, AxiosResponse<Interest>>("/interestList/");
            const { data } = apiReq

            if (data.status === 0) {

                dispatch(AddFormError(data.message))
                return null
            }


            dispatch(addInterests(data.data))

            onSuccess()



            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}



export function postInterests({ interest }: { interest: any }, onSuccess: any) {
    return async (dispatch: any) => {
        try {
            const apiReq = await mainAxios.post<any, AxiosResponse<Interest>>("/interestList/updateInterests", { interest: interest });
            const { data } = apiReq

            if (data.status === 0) {

                dispatch(AddFormError(data.message))
                return null
            }



            onSuccess()



            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}

export function postLocation({ location }: { location: any }, onSuccess: any) {
    return async (dispatch: any) => {
        try {
            const apiReq = await mainAxios.post<any, AxiosResponse<UserData>>("/userLocation/update", { location: { type: 'Point', coordinates: [location.longitude, location.latitude] } });
            const { data } = apiReq

            if (data.status === 0) {

                dispatch(AddFormError(data.message))
                return null
            }

            console.log(data.data)
            dispatch(UpdateUser(data.data))
            onSuccess()



            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}

export function resetPassword({ email }: { email: string }, onSuccess: any) {
    return async (dispatch: any) => {
        try {
            dispatch(CleareFormError())
            const apiReq = await mainAxios.post<any, AxiosResponse<ServerData>>("/userLogin/resetPassword", { user_email: email });
            const { data } = apiReq

            if (data.status === 0) {

                dispatch(AddFormError(data.message))
                return null
            }
            dispatch(SaveOTP(data.otp! + ''))
            onSuccess()


            console.log(data)

            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}




export function changePassword({ email, password, otp }: { email: string, password: string, otp: string }, onSuccess: any) {
    return async (dispatch: any) => {
        try {
            dispatch(CleareFormError())
            const apiReq = await mainAxios.post<any, AxiosResponse<ServerData>>("/userLogin/changePassowrd", { user_email: email, user_password: password, otp: otp });
            const { data } = apiReq

            console.log(data)


            if (data.status === 0) {

                dispatch(AddFormError(data.message))
                return null
            }

            onSuccess()



            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}

export const uploadProfileImage = ({ file }: { file: any }) => {

    return async (dispatch: any) => {


        try {
            const formData = new FormData();

            let localUri: string = file.uri;
            let filename: string = localUri.split('/').pop()!;

            let match = /\.(\w+)$/.exec(filename);
            let type = file.type === 'image' ? `image/${match![1]}` : `video/${match![1]}`;
            const formD: FormDataValue = {
                uri: localUri,
                type: type,
                name: filename,
            }

            formData.append('file', formD);

            const result = await uploadAxios.post<any, AxiosResponse<UserData>>('/profile/image', formData)



            const { data } = result
            console.log(data)
            dispatch(UpdateUser(data.data))

        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }

    }

}




export function updateProfileData({ name, email, mobile }: { name: string, email: string, mobile: string }, onSuccess: any) {
    return async (dispatch: any) => {
        try {

            dispatch(CleareFormError())

            const apiReq = await mainAxios.post<any, AxiosResponse<UserData>>("/profile/data", { user_name: name, user_email: email, user_mobile: mobile });
            const { data } = apiReq

            if (data.status === 0) {

                dispatch(AddFormError(data.message))
                return null
            }



            console.log(data)
            dispatch(UpdateUser(data.data))


            return apiReq || [];
        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
    };
}



const actionCreators = {
    restoreTokenAsync,
    signIn,
    signOut,

    checkEmail,
    checkPhone,
    checkName,
    checkPassward,


    Register,
    Login,
    ValidateOtp,


    ResendOTPAuth,
    ValidateFormRegister,

    CleareFormError,
    checkEmailOrPassword,

    resetPassword,
    changePassword,
    checkPasswords,

    fetchInterest,
    postInterests,
    postLocation,

    uploadProfileImage,

    updateProfileData,
    ValidateEmailOtp,
    SaveOTP

    /*
    tick,
    stop,
    start*/
};

export { actionCreators };
