



const IsUserNameValid = (username: string | null) => {
    if (!username) return false;
    if (username.trim().length > 3) {
        return true;

    }
    return false;
}
const IsValidPassword = (password: string | null) => {

    if (!password) return false;
    if (password.trim().length >= 6) {
        return true;

    }
    return false;
}

const IsValidReferral = (password: any) => {
    if (!password) return false;
    if (password.trim().length >= 6) {
        return true;

    }
    return false;
}

const isVaildEmail = (text: string | null) => {
    if (!text) return false;

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(text) === false) {


        return false;
    }
    return true;
}


const mobilevalidate = (text: string | null) => {
    const reg = /^[0]?[789]\d{9}$/;
    if (!text) return false;
    if (reg.test(text) === false) {

        return false;
    } else {

        return true;
    }
}



export { isVaildEmail, IsValidReferral, IsValidPassword, IsUserNameValid, mobilevalidate }