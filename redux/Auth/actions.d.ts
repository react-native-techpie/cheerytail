
interface ServerResponse {
    data: ServerData
}

interface ServerData {
    message: string
    status: number,
    otp?: string,
    data: { user: User, token: string }
}

interface Interest {
    message: string
    status: number,
    data: []
}
interface UserData {
    message: string
    status: number,
    data: User
}

interface loc {
    type: { type: String },
    coordinates: number[],
}

interface User {
    user_email: string,
    user_mobile: string,
    user_name: string,
    user_interest: string[],
    mobile_verified: boolean,
    location_status: boolean,
    userCurrent_location: loc,
    uri: string,
    _id: string,



}



export {



    ServerResponse,
    ServerData,

    Interest,
    UserData,
    User
};
