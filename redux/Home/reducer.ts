
import {
    LOADING,
    FETCHING_PRODUCTS,
    SET_ALL_PRODUCTS,
    SET_PRODUCT_PAGE,
    RESET_PRODUCT_LIST

} from './types';

import {
    User,
    loc,
    UserData,
    Interest,
    ServerData,
    ServerResponse,

    product,
    IValidObject,
    postProduct,
    ServerProduct
} from '../../datatypes'




// initial state
const initialState = {



    isLoading: false,
    isfetchingProducts: false,


    allProducts: [],
    page: 0,

};

const setPage = ({ state, action }: any) => {

    return {
        ...state,

        page: action.payload

    };
}
const setLoadind = ({ state, action }: any) => {

    return {
        ...state,

        isLoading: action.payload

    };
}

const setFetchingProducts = ({ state, action }: any) => {

    return {
        ...state,

        isfetchingProducts: action.payload


    };
}
const setProducts = ({ state, action }: any) => {
    const previousProducts = state.allProducts

    return {
        ...state,

        allProducts: [...previousProducts, ...action.payload]

    };
}
const resetProducts = ({ state, action }: any) => {
    const previousProducts = state.allProducts

    return {
        ...state,

        allProducts: action.payload

    };
}


function reducer(state = initialState, action: any) {
    switch (action.type) {
        case LOADING:
            return setLoadind({ state, action });
        case FETCHING_PRODUCTS:
            return setFetchingProducts({ state, action });
        case SET_ALL_PRODUCTS:
            return setProducts({ state, action });

        case SET_PRODUCT_PAGE:
            return setPage({ state, action });

        case RESET_PRODUCT_LIST:
            return resetProducts({ state, action });

        default:

            return state;
    }
}

export { reducer };