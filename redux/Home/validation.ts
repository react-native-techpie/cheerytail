
import {
    LOADING,
    FETCHING_PRODUCTS,
    SET_ALL_PRODUCTS

} from './types';

import {


    product,
    IValidObject,

} from '../../datatypes'

export const IsValidName = (username: string | null): IValidObject => {
    if (!username) return { isValid: false, message: 'field can not be empty' };
    if (username.trim().length > 3) {
        return { isValid: true, message: '' };;

    }
    return { isValid: false, message: 'length must be atleast 4 characters' };;
}
export const IsValiImage = (username: string | null): IValidObject => {
    if (!username) return { isValid: false, message: 'please select an image' };
    if (username.trim().length > 30) {
        return { isValid: true, message: '' };;

    }
    return { isValid: false, message: 'please select an image' };;
}

export const IsValidLocation = (loc: number[]): IValidObject => {
    if (!loc) return { isValid: false, message: 'location can not be empty' };
    if (loc.length < 2) {
        return { isValid: false, message: 'invalid location' };

    }
    return { isValid: true, message: '' };
}

const IsValidProduct = (product: product): product => {

    product.title.validation = IsValidName(product.title.value)
    product.discription.validation = IsValidName(product.discription.value)

    product.productType.validation = IsValidName(product.productType.value)
    product.prefered.validation = IsValidName(product.prefered.value)
    product.uri.validation = IsValidName(product.uri.value)

    product.location.validation = IsValidLocation(product.location.value)

    product.isValidProduct = (
        product.location.validation.isValid &&
        product.uri.validation.isValid &&
        product.prefered.validation.isValid &&
        product.productType.validation.isValid &&
        product.discription.validation.isValid &&
        product.title.validation.isValid
    )

    return product;
}


export { IsValidProduct }