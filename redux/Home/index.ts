import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators as actions } from './actions';


import * as types from './types';

function mapStateToProps(state: any) {
    const {
        isLoading,

        isfetchingProducts,

        page,
        allProducts

    } = state.Home;

    const {
        user

    } = state.Auth;
    return {
        isLoading,
        page,
        isfetchingProducts,

        user,
        allProducts
    };
}



export { connect, actions, mapStateToProps, types }