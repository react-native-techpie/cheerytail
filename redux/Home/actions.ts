
import {
    SET_PRODUCT_PAGE,
    LOADING,
    FETCHING_PRODUCTS,
    SET_ALL_PRODUCTS,
    RESET_PRODUCT_LIST
} from './types';

import {
    AxiosError, axios, AxiosResponse,
    uploadAxios, mainAxios
} from '../../api/index';


import { IsValidProduct, IsValidLocation, IsValidName, IsValiImage } from './validation'
import {

    ServerData,
    CustomServerData,
    ProductUpdateParameters,
    product,
    ServerProduct,
    ServerPageListProduct,
    User,
    ServerProductOf,
    Like

} from '../../datatypes'




function setLoading(isLoading: boolean) {


    return {
        type: LOADING,
        payload: isLoading,
    };
}

function setFetchingProducts(isLoading: boolean) {


    return {
        type: FETCHING_PRODUCTS,
        payload: isLoading,
    };
}




function setProducts(products: ServerProductOf<User>[]) {

    return {
        type: SET_ALL_PRODUCTS,
        payload: products,

    };
}



function setProductPage(page: number) {

    return {
        type: SET_PRODUCT_PAGE,
        payload: page,

    };
}


function resetProductsList() {

    return {
        type: RESET_PRODUCT_LIST,
        payload: [],

    };
}



const fetchProducts = (issuPage: number, onSuccess: any) => {

    return async (dispatch: any) => {

        let currentPage = issuPage;

        try {
            if (currentPage === 0) {
                dispatch(resetProductsList())
            }

            dispatch(setFetchingProducts(true));

            const result = await mainAxios.get<any, AxiosResponse<CustomServerData<ServerPageListProduct>>>(`/products/all?since=${issuPage}`)

            dispatch(setFetchingProducts(false));

            const { data } = result

            const { page, list } = data.data

            if (page > currentPage && list.length > 0) {

                dispatch(setProducts(list))
                dispatch(setProductPage(page))



            }

            console.log('list count: ' + list.length)
            console.log('page: ' + page + ' currentPage: ' + currentPage)



            onSuccess()

        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }
        dispatch(setFetchingProducts(false));

    }

}


const likeProduct = (postId: string, onSuccess: any) => {

    return async (dispatch: any) => {

        try {

            const result = await mainAxios.get<any, AxiosResponse<CustomServerData<Like>>>(`/like/${postId}`)


            const { data } = result

            const { liked, matched } = data.data



            onSuccess({ liked, matched })

        } catch (error: any | AxiosError) {

            if (axios.isAxiosError(error)) {
                // Access to config, request, and response
                console.log(error.response?.data)

            } else {
                // Just a stock error
            }

        }


    }

}

const actionCreators = {
    fetchProducts,
    likeProduct
};

export { actionCreators };