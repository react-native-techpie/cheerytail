import React from "react";
import {
  View,
  Image,
  ImageBackground,
} from "react-native";

import styles from "./Styles";
import { back, animals, miniLogo } from '../../../Importer/Auth'

export default function Splash({ navigation }) {


  return (
    <View
      style={styles.container}
    >
      <ImageBackground
        source={back}
        style={styles.backgroundImage}
      >
        <Image
          source={miniLogo}
          style={styles.logo}
        />
        <Image
          source={animals}
          style={styles.bottomImage}
        />
      </ImageBackground>
    </View>
  );
}

