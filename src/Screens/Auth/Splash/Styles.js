import {
    StyleSheet,
} from "react-native";


const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    logo: { height: 170, width: 300 },
    botttomImage: { height: 170, width: 300, marginTop: 60 },
    backgroundImage: { height: "100%", width: "100%", position: "absolute" }


});


export default styles;