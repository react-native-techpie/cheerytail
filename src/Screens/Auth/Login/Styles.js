import {
    StyleSheet,
} from "react-native";


const styles = StyleSheet.create({
    logo: { height: 88, width: 140, marginTop: 20 },
    forgetView: { width: "80%", height: 30, alignItems: "flex-end" },
});


export default styles;