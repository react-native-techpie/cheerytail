import React from "react";
import {
  Image,
} from "react-native";

import { logo } from '../../../Importer/Auth'

import {
  BottomImage,
  Query,
  LoginWith,
  InputPassword,
  Input,
  Container,
  Button,
  BackGround,
  NavLink,
  Spacer,
  ShowFormError

} from "../../../Components/Auth/index";

import styles from "./Styles";

import { connect, actions, mapStateToProps } from '../../../../redux/Auth/index';

export function Login({
  navigation,
  emailOrPassword,
  checkEmailOrPassword,
  checkPassward,
  password,
  responseError,
  CleareFormError,
  Login,

}) {

  console.log(password);
  return (
    <BackGround>
      <Container >
        <Image source={logo} resizeMode='contain' style={styles.logo} />
        <Spacer />
        <Input error={emailOrPassword} value={emailOrPassword.value} setValue={checkEmailOrPassword} placeholder="Email / Mobile Number"></Input>
        <Spacer />
        <InputPassword error={password} value={password.password} setValue={checkPassward} placeholder="Password"></InputPassword>
        <Spacer />
        <NavLink onPress={() => navigation.navigate("ForgotPassword")} >Forgot Password?</NavLink>
        <Spacer />
        <ShowFormError errorMessage={responseError} />
        <Button
          disabled={!password.isValid || !emailOrPassword.isValid}
          onPress={() => Login({ UserName: emailOrPassword.value, Password: password.password }, () => { })} //navigation.navigate("MyTabs")
        >Continue</Button>
        <Spacer />
        <LoginWith />
        <Spacer />
        <Query to={'Sign Up'} query={"New user? "} onPress={() => navigation.navigate("Register")} />
        <Spacer />
        <BottomImage />
      </Container>
    </BackGround>
  );
}


export default connect(
  mapStateToProps,
  actions
)(Login);