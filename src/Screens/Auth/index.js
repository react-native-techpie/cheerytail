import ForgotPassword from './ForgotPassord/Index'
import Login from './Login/Index'
import Register from './Register/Index'
import ResetPassword from './ResetPassword/Index'
import OTPVerify from './OTPVerify/Index'
import Splash from './Splash/Index'

export { ForgotPassword, Login, Register, ResetPassword, OTPVerify, Splash }