import {
    StyleSheet,
} from "react-native";



const styles = StyleSheet.create({
    secondContainer: {
        width: "90%",
        alignItems: "center",
        justifyContent: "flex-start",
        flex: 1,

    },
    scrollView: { width: "90%", alignContent: 'center', flex: 1 },


    passwordView: {
        height: 30,
        width: "100%",
        borderBottomWidth: 2,
        borderColor: "#B42A37",
        flexDirection: "row",
    },
    passwordInput: {
        height: 30,
        width: "80%",
        color: "#B42A37",
        fontSize: 21,
        marginTop: 1,
    },
    eyeTouch: {
        alignItems: "center",
        justifyContent: "center",
        width: "20%",
    },
    buttonView: { width: "100%", alignItems: "center", marginTop: 31 },

    container: {
        backgroundColor: "#FFFF",
        alignItems: "center",
        justifyContent: "center",
    },
    text: {
        fontSize: 16,
        color: "gray",
        marginTop: 10,
    },
    textInput: {
        height: 30,
        width: "100%",
        borderBottomWidth: 2,
        borderColor: "#B42A37",
        paddingLeft: 5,
        color: "#B42A37",
        fontSize: 16,
    },
});



export default styles;