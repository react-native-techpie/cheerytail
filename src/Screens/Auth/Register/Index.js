
import React, { useState } from "react";
import { View, } from "react-native";
import {
  BottomImage,
  Container,
  Button,
  BackGround,

  FlatInput,
  PhoneInput,
  RegisterPasswordInput,
  InfoText,
  Spacer,
  ShowFormError
} from "../../../Components/Auth/index";

import { connect, actions, mapStateToProps } from '../../../../redux/Auth/index';

export function Register({
  navigation,

  email,
  checkEmail,
  checkPassward,
  password,
  phone,
  checkPhone,
  name,
  checkName,
  responseError,
  CleareFormError,


  Register

}) {

  const invalidForm = !name.isValid || !phone.isValid || !email.isValid || !password.isValid

  return (
    <BackGround>
      <Container isHeader={true} >
        <View style={{ width: "90%", alignItems: "flex-start" }}>
          <FlatInput error={name} value={name.name} setValue={checkName} title="Full name"></FlatInput>

          <PhoneInput error={phone} value={phone.phone} setValue={checkPhone} title="Phone"></PhoneInput>

          <FlatInput error={email} title="Email ID" value={email.email} setValue={checkEmail}></FlatInput>

          <RegisterPasswordInput error={password} value={password.password} setValue={checkPassward} title="Password"></RegisterPasswordInput>
          <InfoText>OTP will be sent mob no for verification</InfoText>
          <Spacer />
          <ShowFormError errorMessage={responseError} />
          <Button disabled={invalidForm} onPress={() => {

            Register({ Name: name.name, MobileNo: phone.phone, Email: email.email, Password: password.password, UserRole: 'Doctor', ProfilePicture: "xyz.png", Location: "Baramati", DeviceId: "Redmi" },
              () => { navigation.navigate('OTPVerify') }
            )
          }}>Register</Button>

        </View>
        <BottomImage />
      </Container>
    </BackGround>
  );
}

export default connect(
  mapStateToProps,
  actions
)(Register);