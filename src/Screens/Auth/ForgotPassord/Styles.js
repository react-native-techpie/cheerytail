import {
    StyleSheet,
} from "react-native";

const styles = StyleSheet.create({
    contentView: {
        width: "100%",
        height: 250,
        marginTop: 20,
        alignItems: "center",
        flex: 1,
    },
});


export default styles;