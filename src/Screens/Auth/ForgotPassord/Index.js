
import React, { useState } from "react";
import {
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  View,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { useNavigation } from "@react-navigation/native";
import { GlobalStyle } from "../../../GlobalStyles";
import styles from "./Styles";

import { back, animals } from '../../../Importer/Auth'

export default function ForgotPassword() {
  const { height, width } = Dimensions.get("window");
  const navigation = useNavigation();
  const [isSecureEntry, setIsSecureEntry] = useState(true);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <ScrollView style={GlobalStyle.ScrollView}>
        <View style={GlobalStyle.SecondContainer}>
          <ImageBackground
            source={back}
            // source={require("../assets/Images/back.jpg")}
            imageStyle={{ opacity: 0.6 }}
            style={GlobalStyle.ImageBackground}
          />
          <View style={GlobalStyle.Header}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icon name="chevron-left" size={20} color="black" />
            </TouchableOpacity>
            <Text style={GlobalStyle.HeaderText}>Forgot Pasword</Text>
          </View>
          <View style={GlobalStyle.BodyView}>
            <View style={styles.contentView}>
              <Text style={GlobalStyle.SubHeading}> Forgot Your Password?</Text>
              <TextInput
                keyboardType="default"
                placeholder=" Email id / Mobile Number"
                style={GlobalStyle.TextInput}
              ></TextInput>

              <TouchableOpacity
                onPress={() => navigation.navigate("ResetPassword")}
                style={GlobalStyle.Button}
              >
                <Text style={GlobalStyle.ButtonText}>Continue</Text>
              </TouchableOpacity>
            </View>
            <Image
              source={animals}
              style={GlobalStyle.BottomImage1}
            />
          </View>
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
}

