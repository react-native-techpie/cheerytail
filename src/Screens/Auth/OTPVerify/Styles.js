import {
    StyleSheet,
} from "react-native";



const styles = StyleSheet.create({
    contentContainer: { width: "100%", alignItems: "center" },
    textContainer: { width: "80%", alignItems: "center" },
    content: {
        textAlign: "center",
        marginTop: 20,
        fontSize: 16,
        width: "80%",

    },
    resendView: { flexDirection: "row", marginTop: 22 },

    container: {
        // height:height,
        backgroundColor: "#FFFF",
        alignItems: "center",
        justifyContent: "center",
        // width: width,
    },

});

export default styles;