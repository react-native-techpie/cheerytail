import {
  BottomImage,
  Container,
  Button,
  BackGround,
  Spacer,
  Query,
  OtpInput,
  ShowFormError
} from "../../../Components/Auth/index";
import React from "react";
import { Text } from "react-native";
import { GlobalStyle } from "../../../GlobalStyles";
import styles from "./Styles";
import { connect, actions, mapStateToProps } from '../../../../redux/Auth/index';

export function OTPVerify({
  navigation,
  otp,
  phone,

  ValidateOtp,
  SaveOTP,
  responseError

}) {


  return (

    <BackGround>
      <Container isHeader={true}>
        <Text style={GlobalStyle.SubHeading}>Verify OTP</Text>
        <Text style={styles.content}>
          Please enter 4 Digit code sent to your Mobile number{" "}
        </Text>
        <Spacer />
        <OtpInput value={otp} setValue={SaveOTP} />
        <Spacer />
        <Query query={"If you don't recive the code ? "} to={"Resend"} onPress={() => { }} />
        <Spacer />

        <ShowFormError errorMessage={responseError} />

        <Button disabled={!otp || (otp && otp.length !== 4)}
          onPress={() => ValidateOtp({ MobileNo: phone.phone, Otp: otp })}

        >Continue</Button>
        <Spacer />
        <BottomImage />
      </Container>
    </BackGround>

  );
}

export default connect(
  mapStateToProps,
  actions
)(OTPVerify);