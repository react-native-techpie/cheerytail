import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  View,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  ScrollView,
} from "react-native";
import { GlobalStyle } from "../../../GlobalStyles";
import Icon from "react-native-vector-icons/FontAwesome5";

import styles from "./Styles";
import { back, animals } from '../../../Importer/Auth'

export default function ResetPassword({ navigation }) {
  const [isSecureEntry, setIsSecureEntry] = useState(true);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <ScrollView style={GlobalStyle.ScrollView}>
        <View style={GlobalStyle.SecondContainer}>
          <ImageBackground
            source={back}
            imageStyle={{ opacity: 0.6 }}
            style={GlobalStyle.ImageBackground}
          />
          <View style={GlobalStyle.Header}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icon name="chevron-left" size={20} color="black" />
            </TouchableOpacity>
            <Text style={GlobalStyle.HeaderText}> Reset Password</Text>
          </View>
          <View style={GlobalStyle.BodyContainer}>
            <View style={styles.contentContainer}>
              <Text style={GlobalStyle.SubHeading}>
                {" "}
                Reset Your Password...
              </Text>
              <View style={GlobalStyle.PasswordView}>
                <TextInput
                  keyboardType="default"
                  placeholder=" Password "
                  secureTextEntry={isSecureEntry}
                  style={GlobalStyle.PasswordTextInput}
                ></TextInput>

                <View style={GlobalStyle.EyeView}>
                  <TouchableOpacity
                    onPress={() => {
                      setIsSecureEntry((prev) => !prev);
                    }}
                  >
                    <View>
                      <Icon>
                        {" "}
                        {setIsSecureEntry ? (
                          <Icon name="eye" size={16} />
                        ) : (
                          <Icon name="eye-off-outline" />
                        )}
                      </Icon>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={GlobalStyle.PasswordView}>
                <TextInput
                  keyboardType="default"
                  placeholder="Confirm Password "
                  secureTextEntry={isSecureEntry}
                  style={GlobalStyle.PasswordTextInput}
                ></TextInput>

                <View style={GlobalStyle.EyeView}>
                  <TouchableOpacity
                    onPress={() => {
                      setIsSecureEntry((prev) => !prev);
                    }}
                  >
                    <View>
                      <Icon>
                        {" "}
                        {setIsSecureEntry ? (
                          <Icon name="eye" size={16} />
                        ) : (
                          <Icon name="eye-off-outline" />
                        )}
                      </Icon>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

              <TouchableOpacity
                onPress={() => navigation.navigate("OTPVerify")}
                style={GlobalStyle.Button}
              >
                <Text style={GlobalStyle.ButtonText}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
          <Image
            source={animals}
            style={GlobalStyle.BottomImage1}
          />
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
}

