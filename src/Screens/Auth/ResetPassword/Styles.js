import {
    StyleSheet,
} from "react-native";

const styles = StyleSheet.create({

    contentContainer: {
        width: "100%",
        justifyContent: "space-around",
        marginTop: 20,
        alignItems: "center",
    },
});




export default styles;