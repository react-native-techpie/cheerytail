import React, { useState } from "react";
import {
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useNavigation } from "@react-navigation/native";
import { GlobalStyle } from "../../GlobalStyles";

const { height, width } = Dimensions.get("window");

import { profileIcon, pawIcon, homeIcon, bellIcon, message, search, banner, Camera } from '../../Importer/Home'

import { splashBackGround, animals, fbIcon, miniLogo, logo } from '../../Importer/Auth'
import { doct1 } from '../../Importer/Profile'
import { dog, cat } from '../../Importer/Animal'

export default function Home() {
  const navigation = useNavigation();

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={GlobalStyle.HomeContainer}>
        <View style={GlobalStyle.BannerView}>
          <Image
            source={miniLogo}
            style={GlobalStyle.HeaderLogo}
          />
          <View style={styles.HeaderView}>
            <TouchableOpacity>
              <Image
                source={search}
                style={styles.Search}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                source={message}
                style={styles.Search}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.BodyContainer}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.ScrollView}
          >
            <View style={styles.ScrollViewView}>
              <Image
                source={banner}
                style={styles.BannerImage}
              />
              <View style={styles.OurView}>
                <Text style={styles.TextStyle}>Our Doctors</Text>
                <TouchableOpacity
                  onPress={() => navigation.navigate("DoctorTabs")}
                  style={styles.SeeTouch}
                >
                  <View style={styles.SeeView}>
                    <Text style={styles.SeeText}>See More</Text>
                    <Icon name="chevron-right" size={21} color="red" />
                  </View>
                </TouchableOpacity>
              </View>
              <ScrollView horizontal style={{ width: "100%", height: 140 }}>
                <TouchableOpacity
                  onPress={() => navigation.navigate("DoctorTabs")}
                  style={styles.DoctorView}
                >
                  <Image
                    source={doct1}
                    style={styles.DoctorImage}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate("DoctorTabs")}
                  style={[styles.DoctorView, { marginLeft: 15 }]}
                >
                  <Image
                    source={doct1}
                    style={styles.DoctorImage}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate("DoctorTabs")}
                  style={[styles.DoctorView, { marginLeft: 15 }]}
                >
                  <Image
                    source={doct1}
                    style={styles.DoctorImage}
                  />
                </TouchableOpacity>
              </ScrollView>
              <View style={styles.OurView}>
                <Text style={styles.TextStyle}> Pets in the Community</Text>
                <TouchableOpacity
                  onPress={() => navigation.navigate("UserProfile")}
                  style={styles.SeeTouch}
                >
                  <View style={styles.SeeView}>
                    <Text style={styles.SeeText}>See More</Text>
                    <Icon name="chevron-right" size={21} color="red" />
                  </View>
                </TouchableOpacity>
              </View>
              <ScrollView horizontal style={styles.AnimalScroll}>
                <TouchableOpacity
                  onPress={() => navigation.navigate("UserProfile")}
                  style={styles.ImageButton}
                >
                  <Image
                    source={doct1}
                    style={styles.Animal}
                  />

                  <Text style={styles.AnimalText}>Travis</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate("UserProfile")}
                  style={styles.ImageButton}
                >
                  <Image
                    source={cat}
                    style={styles.Animal}
                  />

                  <Text style={styles.AnimalText}>Cats</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate("UserProfile")}
                  style={styles.ImageButton}
                >
                  <Image
                    source={dog}
                    style={styles.Animal}
                  />
                  <Text style={styles.AnimalText}>Travis</Text>
                </TouchableOpacity>
              </ScrollView>
            </View>
          </ScrollView>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  HeaderView: {
    width: "25%",
    height: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    right: 10,
    paddingTop: 10,
  },
  Search: { height: 35, width: 35 },
  BodyContainer: {
    width: "90%",
    alignItems: "center",
    justifyContent: "flex-end",
    flex: 1,
  },
  ScrollView: { width: "100%" },
  ScrollViewView: { width: "100%", alignItems: "center", paddingTop: 15 },
  BannerImage: { height: 180, width: width - 30, borderRadius: 30 },
  OurView: {
    width: "100%",
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  TextStyle: { fontWeight: "bold", fontSize: 17 },
  SeeTouch: {
    width: "25%",
    height: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  SeeView: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  SeeText: { fontSize: 14, color: "red" },
  DoctorView: {
    height: 130,
    width: 130,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#E0B618",
    borderRadius: 20,
  },
  DoctorImage: { height: 130, width: 100 },
  AnimalScroll: { width: "100%", height: 170 },
  ImageButton: {
    alignItems: "center",
    height: "100%",
    justifyContent: "center",
  },
  Animal: { height: 140, width: 140 },
  AnimalText: { fontSize: 15 },

  container: {
    backgroundColor: "#FFFF",
    alignItems: "center",
    justifyContent: "center",
  },
  Text: {
    fontSize: 16,
    color: "gray",
    marginTop: 10,
  },
  TextInput: {
    height: 30,
    width: "100%",
    borderBottomWidth: 2,
    borderColor: "#B42A37",
    paddingLeft: 5,
    color: "#B42A37",
    fontSize: 16,
  },
});
