import React from "react";
import {
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  View,
  Dimensions,
} from "react-native";

const { height, width } = Dimensions.get("window");

export default function Menu() {
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}></View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    height: height,
    backgroundColor: "#FFFF",
    alignItems: "center",
    width: width,
    justifyContent: "center",
  },
});
