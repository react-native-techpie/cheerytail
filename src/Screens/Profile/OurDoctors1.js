import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import { GlobalStyle } from "../../GlobalStyles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useNavigation } from "@react-navigation/native";
import { doct1, doct2, doct3, doct11, Rating, userProfile } from '../../Importer/Profile'

const { height, width } = Dimensions.get("window");

export default function OurDoctors() {
  const navigation = useNavigation();

  const [isSecureEntry, setIsSecureEntry] = useState(true);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={GlobalStyle.SecondContainer}>
        <View style={GlobalStyle.Header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="chevron-left" size={37} color="black" />
          </TouchableOpacity>
          <Text style={GlobalStyle.HeaderText}>Our Doctors</Text>
        </View>
        <View style={GlobalStyle.SecondContainer}>
          <ScrollView style={{ width: "100%" }}>
            <View style={styles.BodyContainer}>
              <View style={styles.ImageView}>
                <Image
                  source={doct11}
                  style={styles.Image}
                />
              </View>
              <View style={styles.ContentView}>
                <View style={styles.ContentHeader}>
                  <Text style={styles.ContentHeaderText}>
                    Dr. Kennth morgan
                  </Text>
                  <Text style={styles.ContentBody}>
                    Akshay Nagar 1 st Block 1st Cross.
                  </Text>
                  <Image
                    source={Rating}
                    style={styles.StarStyle}
                  />
                </View>
                <View style={styles.ReadView}>
                  <View style={styles.ReadView2}>
                    <Text style={styles.ExpText}>5 Years Exp</Text>
                  </View>
                  <TouchableOpacity style={styles.ReadButton}>
                    <View style={styles.ReadViewStyle}>
                      <Text style={styles.ReadTextStyle}>Read More</Text>
                      <Icon name="chevron-right" size={21} color="red" />
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  BodyContainer: { width: "100%", alignItems: "center" },
  ImageView: {
    height: "77%",
    width: "100%",
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "#E0B618",
  },

  Image: { width: width - 30, resizeMode: "cover" },
  ContentView: {
    width: "95%",
    height: 120,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderColor: "gray",
  },
  ContentHeader: {
    width: "60%",
    justifyContent: "space-around",
    height: "100%",
    padding: 5,
  },
  ContentHeaderText: { fontWeight: "bold", fontSize: 16 },
  ContentBody: { fontSize: 12 },
  StarStyle: { height: 30, width: 90 },
  ReadView: {
    width: "30%",
    justifyContent: "space-around",
    height: "100%",
  },
  ReadView2: {
    height: 30,
    width: 90,
    backgroundColor: "#F5B7B1",
    borderRadius: 20,
    justifyContent: "center",
  },
  ExpText: {
    color: "white",
    textAlign: "center",
    fontWeight: "bold",
  },
  ReadButton: {
    width: "80%",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  ReadViewStyle: {
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  ReadTextStyle: { fontSize: 14, color: "red" },

});
