import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import OurDoctors from "./OurDoctors1";


import { doct1, doct2, doct3, doct11 } from '../../Importer/Profile'

const Tab = createBottomTabNavigator();

export default function DoctorTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          backgroundColor: "#FFFF",
          height: 100,
        },
      }}
    >
      <Tab.Screen
        name="OurDoctors"
        component={OurDoctors}
        elevation="10"
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.ViewStyle}>
              <View style={styles.ViewStyle2}>
                <Image
                  source={doct1}
                  style={styles.Image}
                />
              </View>
              <Text style={styles.Text}>Dr. Kennth</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="SecondDoctor"
        component={OurDoctors}
        elevation="10"
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.ViewStyle}>
              <View style={styles.ViewStyle2}>
                <Image
                  source={doct2}
                  style={styles.Image}
                />
              </View>
              <Text style={styles.Text}>Dr. Kennth</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="ThirdDoctor"
        component={OurDoctors}
        elevation="10"
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.ViewStyle}>
              <View style={styles.ViewStyle2}>
                <Image
                  source={doct3}
                  style={styles.Image}
                />
              </View>
              <Text style={styles.Text}>Dr. Kennth</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="FourthDoctor"
        component={OurDoctors}
        elevation="10"
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.ViewStyle}>
              <View style={styles.ViewStyle2}>
                <Image
                  source={doct11}
                  style={styles.Image}
                />
              </View>
              <Text style={styles.Text}>Dr. Kennth</Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  ViewStyle: { alignItems: "center", height: "100%", width: 90 },
  ViewStyle2: {
    height: 80,
    width: 80,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: "red",
    alignItems: "center",
  },
  Image: { height: 70, width: 50 },
  Text: { fontSize: 11 },
});
