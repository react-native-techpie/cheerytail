import React, { useState } from "react";
import {
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useNavigation } from "@react-navigation/native";
import { GlobalStyle } from "../../GlobalStyles";

import { Rating, userProfile } from '../../Importer/Profile'

const { height, width } = Dimensions.get("window");

export default function UserProfile() {
  const navigation = useNavigation();
  const [isSecureEntry, setIsSecureEntry] = useState(true);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={GlobalStyle.HomeContainer}>
        <View style={GlobalStyle.Header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="chevron-left" size={37} color="black" />
          </TouchableOpacity>
          <Text style={GlobalStyle.HeaderText}>Profile</Text>
        </View>
        <View style={GlobalStyle.SecondContainer}>
          <ScrollView style={{ width: "100%" }}>
            <View style={{ width: "100%", alignItems: "center" }}>
              <View style={styles.BodyContainer}>
                <Image
                  source={userProfile}
                  style={styles.ImageStyle}
                />
              </View>
              <View style={styles.ContentView}>
                <View style={styles.ContentView2}>
                  <Text style={GlobalStyle.ContentHeader}>Rakesh S</Text>
                  <Text style={styles.ContentText}>
                    Akshay Nagar 1 st Block 1st Cross.
                  </Text>
                  <TouchableOpacity style={styles.ReadTouch}>
                    <View style={styles.ReadView}>
                      <Text style={styles.ReadText}>Read More</Text>
                      <Icon name="chevron-right" size={21} color="red" />
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.ViewStyle}></View>
              </View>
              <View style={styles.OurView}>
                <Text style={styles.OurText}>Our Pets</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  BodyContainer: {
    height: "70%",
    width: "100%",
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "#E0B618",
  },
  ImageStyle: { width: width - 30, resizeMode: "cover" },
  ContentView: {
    width: "95%",
    height: 120,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderColor: "gray",
  },
  ContentView2: {
    width: "60%",
    justifyContent: "space-around",
    height: "100%",
    padding: 5,
  },
  ContentHeader: { fontWeight: "bold", fontSize: 17 },
  ContentText: { fontSize: 12 },
  ReadTouch: {
    width: "80%",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  ReadView: {
    width: "100%",
    alignItems: "center",
    flexDirection: "row",
  },
  ReadText: { fontSize: 14, color: "red" },
  ViewStyle: {
    width: "30%",
    justifyContent: "space-around",
    height: "100%",
  },
  OurView: {
    width: "90%",
    height: 30,
    marginTop: 11,
    justifyContent: "center",
  },
  OurText: { fontSize: 17 },
});
