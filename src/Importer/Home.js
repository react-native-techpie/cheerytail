import bellIcon from "../../assets/Home/Bell.png"
import homeIcon from "../../assets/Home/Home.png"
import pawIcon from "../../assets/Home/iconNew.png"
import profileIcon from "../../assets/Home/Profile.png"
import message from "../../assets/Home/message.png"
import search from "../../assets/Home/search.png"
import banner from "../../assets/Home/banner.jpg"
import Camera from "../../assets/Home/Camera.png"

export { profileIcon, pawIcon, homeIcon, bellIcon, message, search, banner, Camera }