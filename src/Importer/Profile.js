import doct1 from "../../assets/Doctor/doct1.png"
import doct2 from "../../assets/Doctor/doct2.png"
import doct3 from "../../assets/Doctor/doct3.png"
import doct11 from "../../assets/Doctor/doct11.png"
import Rating from "../../assets/Doctor/Rating.png"
import userProfile from "../../assets/Profile/ProfileImg.png"


export { doct1, doct2, doct3, doct11, Rating, userProfile }