import back from "../../assets/Splash/back.jpg"
import animals from "../../assets/Splash/animals.png"
import fbIcon from "../../assets/Images/fb.png"
import logo from "../../assets/Images/Logo.png"
import miniLogo from "../../assets/Images/miniLogo.png"

export { back, animals, fbIcon, miniLogo, logo }