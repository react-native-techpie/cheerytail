import React from "react";
import { Dimensions, TouchableOpacity, Text, View } from "react-native";

import styles from './Styles'


export default function Button({ disabled, children, onPress }) {

  return (
    <View style={styles.container}>
      <TouchableOpacity disabled={disabled} onPress={onPress} style={[styles.buttonStyle, { opacity: disabled ? 0.5 : 1 }]}>
        <Text style={styles.buttonText}>{children}</Text>
      </TouchableOpacity>

    </View>
  );
}

