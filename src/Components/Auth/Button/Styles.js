import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    buttonStyle: {
        height: 50,
        width: "80%",
        borderRadius: 10,
        elevation: 3,
        backgroundColor: "red",
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText: {
        color: "white",
        fontSize: 19,
    },
    container: { flexDirection: 'row', justifyContent: 'center', margin: 5, alignItems: "center", width: '100%' }
});


export default styles;