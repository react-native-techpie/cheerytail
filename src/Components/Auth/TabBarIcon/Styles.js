import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    container: {

        alignItems: "center",

    },
    imageStyle: {
        width: 46,
        height: 46,
        marginBottom: -10


    },

    textStyle: {
        fontSize: 12,
        textAlign: "center",


    }
});


export default styles;