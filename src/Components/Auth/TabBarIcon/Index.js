
import React from 'react';
import { View, Text, Image } from 'react-native';
import styles from "./Styles"


const TabBarIcon = ({ focused, uri, icon, children }) => {


    return (
        <View style={styles.container}>
            {uri && <Image style={[styles.imageStyle, { tintColor: focused ? "red" : "#B5AEAD" }]} source={uri} resizeMode="contain" />}
            {icon}
            <Text style={[styles.textStyle, { tintColor: focused ? "red" : "#B5AEAD" }]}>
                {children}
            </Text>
        </View>
    )
}

export default TabBarIcon;