import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    viewStyle: {
        width: "37%",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    touchStyle: {
        height: 50,
        width: 50,
        borderColor: "#F5B7B1",
        borderRadius: 50,
        borderWidth: 1.5,
        backgroundColor: "white",
        elevation: 4,
        alignItems: "center",
        justifyContent: "center",
    },
});


export default styles;