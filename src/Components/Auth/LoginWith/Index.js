import React from "react";
import { View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import styles from "./Styles"

export default function LoginWith({ text }) {
  return (
    <View style={styles.viewStyle}>
      <TouchableOpacity style={styles.touchStyle}>
        <Icon name="facebook-f" color="#21618C" size={35} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.touchStyle}>
        <Icon name="google-plus-g" color="red" size={33} />
      </TouchableOpacity>
    </View>
  );
}

