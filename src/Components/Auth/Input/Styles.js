import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    inputStyle: {
        height: 50,
        marginHorizontal: 10,
        width: '80%',
        borderWidth: 1.5,
        borderRadius: 10,
        borderColor: "#F5B7B1",
        elevation: 3,
        backgroundColor: "white",
        paddingLeft: 10,
        fontSize: 16,
    },
});


export default styles;