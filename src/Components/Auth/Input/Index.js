import React, { useState } from "react";
import { TextInput } from "react-native";
import styles from "./Styles"
import { ShowValidaionMessage } from '../Error/Index'

export default function Input({
  value,
  setValue,
  placeholder,
  secureTextEntry,
  keyboardType,
  error
}) {
  const [firstLoaded, setFirstLoaded] = useState(true);
  return (
    <>

      <TextInput
        style={styles.inputStyle}
        placeholder={placeholder}
        value={value}
        secureTextEntry={secureTextEntry}
        keyboardType={keyboardType}
        onChangeText={(v) => {
          setFirstLoaded(false);
          setValue(v);
        }}
      ></TextInput>
      {ShowValidaionMessage({ errorMessage: error.error, isValid: error.isValid, firstLoaded })}
    </>
  );
}

