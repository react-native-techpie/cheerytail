import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import styles from "./Styles"

export function SignUp({ onPress, to, query }) {
  return (
    <View style={styles.container}>
      <Text style={styles.queryText}>{query}</Text>
      <TouchableOpacity
        onPress={onPress}
        style={styles.touchableContainer}
      >
        <Text style={styles.toText}>{to}</Text>
      </TouchableOpacity>
    </View>
  );
}

