import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "center"
    },
    queryText: {
        fontSize: 17,
        color: "black",

    },
    toText: {
        fontSize: 17,
        color: "red",

    },
    touchableContainer: { borderBottomWidth: 1, borderColor: "red" }
});


export default styles;