import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 60,
        backgroundColor: "pink",
        elevation: 4,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        paddingLeft: 10,
    },
    header: {
        width: '100%',
        height: 50,
        backgroundColor: "pink",
        elevation: 4,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        paddingLeft: 20,
        alignContent: 'center',
    },
    headerText: { fontSize: 20, fontWeight: "bold", marginLeft: 10 },
});


export default styles;