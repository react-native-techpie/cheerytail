import React from "react";
import { View, Text, Dimensions, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import styles from "./Styles"

export default function Header({ children, navigation }) {
  return (
    <View
      style={styles.header}
    >
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Icon name="chevron-left" size={38} color="black" />
      </TouchableOpacity>
      <Text style={styles.headerText}> {children}</Text>
    </View>
  );
}

