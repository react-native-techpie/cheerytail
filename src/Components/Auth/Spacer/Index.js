import React from "react";
import { View } from "react-native";
import styles from "./Styles"



export default function Spacer({ children }) {
  return (
    <View style={styles.SpacerStyle}>
      {children}
    </View>
  );
}
