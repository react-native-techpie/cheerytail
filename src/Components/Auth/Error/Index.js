
import React from 'react';
import { View, Text } from 'react-native'
import styles from './Styles'

const ShowValidaionMessage = ({ style, isValid, firstLoaded, errorMessage, color = 'red' }) => {
    return (!isValid && !firstLoaded ? (
        ShowFormError({ errorMessage, color, style })
    ) : null
    )
}

const ShowFormError = ({ style, errorMessage, color = 'red' }) => {
    return (errorMessage && errorMessage != '' ? (
        <View style={[styles.container, style]}>


            <Text style={[styles.errorMessage, { color: color }]}>{errorMessage ? errorMessage : ""}</Text>
        </View>
    ) : null

    )
}



ShowValidaionMessage.defaultProps = {
    color: 'red'
}

export { ShowValidaionMessage, ShowFormError }