import {
    StyleSheet,
} from "react-native";


const styles = StyleSheet.create({

    errorMessage: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

        marginLeft: 18,
        marginRight: 18,
        marginTop: 5,
        color: 'red',
        textAlign: 'center',
        alignContent: 'center',


    },
    container: {

        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

        width: '90%',
        alignContent: 'center',

    }
})

export default styles;