import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from "./Styles"

const NavLink = ({ onPress, children }) => {
    return (
        <View style={styles.forgetView}>

            <TouchableOpacity
                onPress={onPress}
            >
                <Text style={styles.registerText}>{children}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default NavLink;
