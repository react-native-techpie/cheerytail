import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    registerView: {
        flexDirection: "row",
        width: "42%",
        justifyContent: "space-between",
        height: "3%",
        marginTop: 0,
    },
    registerText: {
        fontSize: 17,
        color: "black",
    },
    register: {
        fontSize: 17,
        color: "red",
    },
    touchableContainer: { borderBottomWidth: 1, height: 27, borderColor: "red" }
});


export default styles;