import React from "react";
import { Image, View } from "react-native";
import styles from './Styles'
import { animals } from '../../../Importer/Auth'

export function BottomImage() {
  return (
    <View style={styles.container}>

      <Image
        source={animals}
        style={styles.bottomImage}
        resizeMode='contain'
      />

    </View>
  );
}

