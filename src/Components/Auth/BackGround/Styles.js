import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    background: { height: "100%", width: "100%", position: 'absolute' },
    container: {
        height: "100%",
        width: "100%",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: 'transparent'
    },

});


export default styles;