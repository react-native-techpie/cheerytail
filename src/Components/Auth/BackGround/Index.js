import React from 'react';
import { ImageBackground, KeyboardAvoidingView } from 'react-native';
import styles from './Styles'
import { back } from '../../../Importer/Auth'

export default function BackGround({ children }) {
  return (
    <KeyboardAvoidingView


      style={styles.background}
    >
      <ImageBackground
        source={back}
        style={styles.background}
      >

        {children}
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}

