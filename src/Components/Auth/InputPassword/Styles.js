import { StyleSheet } from 'react-native';



const styles = StyleSheet.create({
    inputStyle: {
        height: 50,
        width: "80%",
        borderWidth: 1.5,
        borderRadius: 10,
        borderColor: "#F5B7B1",
        elevation: 3,
        backgroundColor: "white",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    textInputStyle: {
        height: "100%",
        width: "80%",
        paddingLeft: 10,
        fontSize: 16,
        borderRadius: 10,
        backgroundColor: "white",
    },
    eyeView: {
        height: "100%",
        width: "18%",
        alignItems: "center",
        justifyContent: "center",
    },
});

export default styles;