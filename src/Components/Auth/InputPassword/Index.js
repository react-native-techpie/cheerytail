import React, { useState } from "react";
import {
  TextInput,
  View,
  TouchableOpacity,
} from "react-native";

import styles from "./Styles"
import { ShowValidaionMessage } from '../Error/Index'
import { FontAwesome } from '@expo/vector-icons'

export default function InputPassword({
  value,
  setValue,
  placeholder,

  keyboardType,
  error
}) {
  const [isSecureEntry, setIsSecureEntry] = useState(true);
  const [firstLoaded, setFirstLoaded] = useState(true);
  console.log(error);

  return (
    <>

      <View style={styles.inputStyle}>
        <TextInput
          style={styles.textInputStyle}
          placeholder={placeholder}
          secureTextEntry={isSecureEntry}
          value={value}

          keyboardType={keyboardType}
          onChangeText={(v) => {
            setFirstLoaded(false)
            setValue(v);
          }}
        ></TextInput>
        <View style={styles.eyeView}>
          <TouchableOpacity
            onPress={() => {
              setIsSecureEntry((prev) => !prev);
            }}
          >

            {isSecureEntry ? (
              <FontAwesome name="eye" size={18} color="black" />
            ) : (
              <FontAwesome name="eye-slash" size={18} color="black" />
            )}

          </TouchableOpacity>
        </View>

      </View>
      {ShowValidaionMessage({ errorMessage: error.error, isValid: error.isValid, firstLoaded })}
    </>

  );
}
