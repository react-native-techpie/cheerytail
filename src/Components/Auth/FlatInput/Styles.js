import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    text: {
        fontSize: 16,
        color: "gray",
        marginTop: 10,

    },
    textInput: {
        height: 30,

        width: '100%',
        borderBottomWidth: 2,
        borderColor: "#B42A37",
        paddingLeft: 5,
        color: "#B42A37",
        fontSize: 16,

    },

    countryInput: {
        height: 30,
        width: "14%",
        borderBottomWidth: 2,
        borderColor: "#B42A37",
        color: "#B42A37",
        fontSize: 21,
        textAlign: "center",
    },
    mobile: {
        height: 30,
        flex: 1,
        marginLeft: 20,

        borderBottomWidth: 2,
        borderColor: "#B42A37",
        color: "#B42A37",
        fontSize: 21,
        textAlign: "center",
        textAlign: "left",
    },
    mobileView: {
        height: 45,
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    inputStyle: {
        height: 50,
        width: "100%",

        borderColor: "#F5B7B1",

        flexDirection: "row",

    },
    eyeView: {
        height: "100%",
        width: "18%",
        position: 'absolute',
        right: 0,
        top: -20,
        alignItems: "center",
        justifyContent: "center",
    },
});


export default styles;