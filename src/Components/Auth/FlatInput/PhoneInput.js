import React, { useState } from 'react'
import { View, Text, TextInput } from 'react-native'
import styles from './Styles'
import { ShowValidaionMessage } from '../Error/Index'

const PhoneInput = ({ title, placeholder, setValue, value, error }) => {

    const [firstLoaded, setFirstLoaded] = useState(true);
    return (
        <>

            <Text style={styles.text}>{title} </Text>
            <View style={styles.mobileView}>
                <TextInput
                    keyboardType="phone-pad"
                    maxLength={3}
                    value={'+91'}
                    editable={false}
                    style={styles.countryInput}
                />
                <TextInput
                    style={styles.mobile}
                    placeholder={placeholder}
                    value={value}
                    keyboardType={'phone-pad'}
                    onChangeText={(v) => {
                        setFirstLoaded(false)
                        setValue(v);
                    }}
                />
            </View>
            {ShowValidaionMessage({ errorMessage: error.error, isValid: error.isValid, firstLoaded })}
        </>
    )
}

export default PhoneInput
