import React, { useState } from "react";
import { TextInput, Text } from "react-native";
import styles from "./Styles"
import PhoneInput from "./PhoneInput"
import PasswordInput from "./PasswordInput"
import InfoText from "./InfoText";
import { ShowValidaionMessage } from '../Error/Index'

export default function Input({
  value,
  setValue,
  placeholder,
  secureTextEntry,
  keyboardType,
  title,
  error
}) {

  const [firstLoaded, setFirstLoaded] = useState(true);
  return (
    <>

      <Text style={styles.text}>{title} </Text>
      <TextInput
        keyboardType="default"
        style={styles.textInput}
        placeholder={placeholder}
        value={value}
        secureTextEntry={secureTextEntry}
        keyboardType={keyboardType}
        onChangeText={(v) => {
          setFirstLoaded(false)
          setValue(v);
        }}
      />
      {ShowValidaionMessage({ errorMessage: error.error, isValid: error.isValid, firstLoaded })}


    </>

  );
}

export { PhoneInput, PasswordInput, InfoText }

