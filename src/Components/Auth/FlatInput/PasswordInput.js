import React, { useState } from "react";
import {
    TextInput,
    View,
    Text,
    TouchableOpacity,
} from "react-native";


import styles from "./Styles"
import { ShowValidaionMessage } from '../Error/Index'
import { FontAwesome } from '@expo/vector-icons'

export default function PasswordInput({
    value,
    setValue,
    placeholder,
    keyboardType,
    title,
    error
}) {
    const [isSecureEntry, setIsSecureEntry] = useState(true);
    const [firstLoaded, setFirstLoaded] = useState(true);
    return (
        <>

            <Text style={styles.text}>{title} </Text>
            <View style={styles.inputStyle}>
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    secureTextEntry={isSecureEntry}
                    value={value}

                    keyboardType={keyboardType}
                    onChangeText={(v) => {
                        setFirstLoaded(false)
                        setValue(v);
                    }}
                ></TextInput>
                <View style={styles.eyeView}>
                    <TouchableOpacity
                        onPress={() => {
                            setIsSecureEntry((prev) => !prev);
                        }}
                    >
                        {isSecureEntry ? (
                            <FontAwesome name="eye" size={18} color="black" />
                        ) : (
                            <FontAwesome name="eye-slash" size={18} color="black" />
                        )}

                    </TouchableOpacity>
                </View>
            </View>
            {ShowValidaionMessage({ errorMessage: error.error, isValid: error.isValid, firstLoaded })}
        </>
    );
}
