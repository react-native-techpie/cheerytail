import React from 'react'
import { View, Text } from 'react-native'
import styles from './Styles'

const InfoText = ({ children }) => {
    return (
        <Text style={styles.text}>
            {children}
        </Text>
    )
}

export default InfoText
