
import BackGround from "./BackGround/Index";
import Button from "./Button/Index";
import Container from "./Container/Index";
import Header from "./Header/Index";
import Input from "./Input/Index";
import InputPassword from "./InputPassword/Index";
import LoginWith from "./LoginWith/Index";
import { SignUp as Query } from "./SignUp/Index";
import { BottomImage } from "./BottomImage/Index";
import TabBarIcon from "./TabBarIcon/Index";
import NavLink from "./NavLink/Index";
import FlatInput, { PhoneInput, PasswordInput as RegisterPasswordInput, InfoText } from "./FlatInput/Index";
import Spacer from "./Spacer/Index"
import { OtpInput } from "./OtpInput/Index";
import { ShowValidaionMessage, ShowFormError } from './Error/Index'


export {
    BackGround,
    Button,
    Container,
    Header,
    Input,
    InputPassword,
    LoginWith,
    Query,
    BottomImage,
    TabBarIcon,
    NavLink,
    FlatInput,
    PhoneInput,
    RegisterPasswordInput,
    InfoText,
    Spacer,
    OtpInput,
    ShowValidaionMessage,
    ShowFormError
}