import React from 'react';
import { ScrollView, StatusBar, View } from 'react-native';
import styles from './Styles'
import Header from '../Header/Index'

import { useRoute, useNavigation } from '@react-navigation/native';



const Container = ({ children, width, alignItems, isHeader }) => {
    const route = useRoute();
    const navigation = useNavigation();
    return (
        <>
            <StatusBar
                animated={true}
                backgroundColor="pink"
                barStyle={'default'}
                showHideTransition={'fade'}
                hidden={false} />

            {isHeader && <Header navigation={navigation}>{route.name}</Header>}
            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }} style={styles.scrollView}>
                <View style={[styles.container, { width, alignItems }]} >
                    {children}
                </View>
            </ScrollView>
        </>
    );
}
Container.defaultProps = {
    width: '100%',
    alignItems: 'center',
    isHeader: false,
}

export default Container;

