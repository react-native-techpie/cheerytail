import { StyleSheet, StatusBar } from 'react-native';

const styles = StyleSheet.create({

    container: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        paddingTop: 10,


        backgroundColor: 'transparent',
        flex: 1,
    },
    scrollView: {
        width: "100%",

    }
});


export default styles;