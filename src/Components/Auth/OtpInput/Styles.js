import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    root: { padding: 2, minHeight: 20, alignItems: "center", justifyContent: "center", width: '100%' },
    title: { textAlign: "center", fontSize: 20 },
    codeFieldRoot: {
        marginTop: 1,
        width: '80%',
        marginLeft: "auto",
        marginRight: "auto",
        height: 50,
    },
    cellRoot: {
        height: 50,
        width: 50,
        borderBottomWidth: 2,
        borderColor: "#B42A37",
        color: "#B42A37",
        fontSize: 21,
        textAlign: "center",

    },
    cellText: {
        color: "#000",
        fontSize: 26,
        textAlign: "center",
    },
    focusCell: {
        borderBottomColor: "#007AFF",
        borderBottomWidth: 0.0,
    },
});


export default styles;