
import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

import Home from "./Home"

import { DoctorTabs, UserProfile } from '../Screens/Profile/index'



export default function Main() {
    return (
        <Stack.Navigator
            screenOptions={{ headerShown: null }}
        >
            <Stack.Screen name="MyTabs" component={Home} />
            <Stack.Screen name="DoctorTabs" component={DoctorTabs} />
            <Stack.Screen name="UserProfile" component={UserProfile} />
        </Stack.Navigator>
    );
}