import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { ForgotPassword, Login, Register, ResetPassword, OTPVerify } from '../Screens/Auth/index'

const Stack = createStackNavigator();

export default function Auth() {
    return (
        <Stack.Navigator
            initialRouteName="Login"
            screenOptions={{ headerShown: null }}
        >
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="OTPVerify" component={OTPVerify} />
            <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
            <Stack.Screen name="ResetPassword" component={ResetPassword} />

        </Stack.Navigator>
    );
}


