import React from 'react';
import Auth from './Auth';
import Main from './Main';

import { connect, actions, mapStateToProps } from '../../redux/Auth/index';

export function Root({
  user

}) {

  return (
    !user ? <Auth /> : <Main />
  );
}

export default connect(
  mapStateToProps,
  actions
)(Root);