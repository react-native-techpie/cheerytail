import React from 'react';
import { Image, View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { TabBarIcon } from '../Components/Auth/index'
import { Notification, Paw, Menu, Profile, Home } from '../Screens/Home/index';
import { profileIcon, pawIcon, homeIcon, bellIcon, message, search } from '../Importer/Home'
const Tab = createBottomTabNavigator();
import { FontAwesome } from '@expo/vector-icons';

const tabBarOptions = {
    headerShown: false,
    tabBarShowLabel: false,
    tabBarStyle: {
        backgroundColor: "#FFFF",
        height: 60,
        elevation: 10,
    },
}



export default function HomeTabs() {
    return (
        <Tab.Navigator
            screenOptions={tabBarOptions}
        >
            <Tab.Screen name="Home" component={Home} elevation="10"
                options={{
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} uri={homeIcon} >Home</TabBarIcon>
                }}
            />
            <Tab.Screen name="Bell" component={Notification} elevation="10"
                options={{
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} uri={bellIcon} >Notification</TabBarIcon>
                }}
            />
            <Tab.Screen name="Spaa" component={Paw} elevation="10"
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                justifyContent: "center",
                                alignItem: "center",
                                backgroundColor: "red",
                                height: 45,
                                width: 45,
                                elevation: 5,
                                borderRadius: 40,
                            }}
                        >
                            <Image
                                source={pawIcon}
                                resizeMode="contain"
                                style={{
                                    width: 45,
                                    height: 45,
                                }}
                            />
                        </View>
                    ),
                }}
            />
            <Tab.Screen name="Profile" component={Profile} elevation="10"
                options={{
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} uri={profileIcon} >Profile</TabBarIcon>
                }}
            />
            <Tab.Screen name="Menu" component={Menu} elevation="10"
                options={{
                    tabBarIcon: ({ focused }) => <TabBarIcon
                        icon={
                            <FontAwesome name="image" size={24} color={focused ? "red" : "#B5AEAD"} />}
                        focused={focused}
                    >
                        Moments
                    </TabBarIcon>
                }}
            />
        </Tab.Navigator>
    );
}

