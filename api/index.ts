import axios, { AxiosError, AxiosResponse } from "axios";


import {


  url
} from './uri'




const mainAxios = axios.create({
  baseURL: url,
  headers: { 'Content-Type': 'application/json' },

});




const uploadAxios = axios.create({
  baseURL: url,//'',
  headers: { 'Content-Type': 'multipart/form-data' },

  //'http:206.189.139.51'
});


axios.interceptors.request.use(request => {
  console.log('Starting Request', JSON.stringify(request, null, 2))
  return request
})

axios.interceptors.response.use(response => {
  console.log('Response:', JSON.stringify(response, null, 2))
  return response
})

const SetAuthorizationHeader = (token: string) => {

  console.log(token)

  axios!.defaults!.headers!.common['Authorization'] = `Bearer ${token}`


  mainAxios!.defaults!.headers!.common['Authorization'] = `Bearer ${token}`
  uploadAxios!.defaults!.headers!.common['Authorization'] = `Bearer ${token}`
}

export {
  SetAuthorizationHeader,
  mainAxios,
  uploadAxios,
  AxiosError,
  axios,
  AxiosResponse,


};